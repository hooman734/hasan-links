import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'

export default function Home() {
    return (
        <div className={styles.container}>
            <Head>
                <title>«محمد حسن صفریان»</title>
                <meta name="description"
                      content="داوطلب عضویت در نهمین دوره انتخابات هیات مدیره نظام مهندسی ساختمان استان گلستان"/>
                <link rel="preconnect" href="https://fonts.googleapis.com"/>
                <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin/>
                <link href="https://fonts.googleapis.com/css2?family=Lalezar&display=swap" rel="stylesheet"/>
                <link rel="preconnect" href="https://fonts.googleapis.com"/>
                <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin/>
                <link href="https://fonts.googleapis.com/css2?family=Markazi+Text&display=swap"
                      rel="stylesheet"/>
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"
                      integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ=="
                      crossOrigin="anonymous" referrerpolicy="no-referrer"/>
                <link rel="preconnect" href="https://fonts.googleapis.com"/>
                <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin/>
                <link href="https://fonts.googleapis.com/css2?family=Noto+Naskh+Arabic&display=swap"
                      rel="stylesheet"/>
            </Head>

            <main className={styles.main}>
                <h1 className={styles.title}>
                    محمد حسن صفریان
                </h1>

                <p className={styles.description}>
                    <ul>
                        <li>
                            داوطلب عضویت در نهمین دوره انتخابات هیات مدیره نظام مهندسی ساختمان استان گلستان
                        </li>
                        <li>
                            مهندس پایه ارشد در رشته عمران
                        </li>
                        <li>
                            معاون عمرانی دانشگاه علوم کشاورزی و منابع طبیعی گرگان
                        </li>
                    </ul>
                </p>

                <div className={styles.vimeo} >
                <iframe src="https://player.vimeo.com/video/612063394?h=5c94fbd7ed&amp;title=0&amp;byline=0&amp;portrait=0&amp;speed=0&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" width="100%" height="100%" frameBorder="0" allow="autoplay; fullscreen; picture-in-picture" allowFullScreen title="Safarian"></iframe>
                </div>

                <div className={styles.grid}>
                    <a href="https://www.instagram.com/invites/contact/?i=o96l0v33wnxz&utm_content=mn2vj23"
                       className={styles.card}>
                        <h2 className={styles.RTL}><i className="fab fa-instagram"></i> اینستاگرام </h2>
                        <small  className={styles.CTR}>لطفا با کلیک روی ایکون عضو شوید <i
                            className="fas fa-info-circle"></i></small>
                    </a>

                    <a href="https://t.me/joinchat/zMYJdTGEw2hjMmU0" className={styles.card}>
                        <h2 className={styles.RTL}><i className="fab fa-telegram-plane"></i> تلگرام </h2>
                        <small className={styles.CTR}>لطفا با کلیک روی ایکون عضو شوید <i
                            className="fas fa-info-circle"></i></small>
                    </a>

                    <a
                        href="https://chat.whatsapp.com/I8JA9kzVWd39tc5Zy1uUyZ"
                        className={styles.card}
                    >
                        <h2 className={styles.RTL}><i className="fab fa-whatsapp"></i> واتساپ </h2>
                        <small className={styles.CTR}>لطفا با کلیک روی ایکون عضو شوید <i
                            className="fas fa-info-circle"></i></small>
                    </a>

                    <a
                        href="https://chat.whatsapp.com/Cxt8R5x446kIPh80dZXnt8"
                        className={styles.card}
                    >

                        <h2 className={styles.RTL}><i className="fab fa-whatsapp"></i> واتساپ دوم</h2>

                        <small className={styles.CTR}>لطفا با کلیک روی ایکون عضو شوید <i
                            className="fas fa-info-circle"></i></small>
                    </a>
                </div>
            </main>

            <footer className={styles.footer}>
                {/*<p>*/}
                {/*    عرض سلام و احترام خدمت همکاران محترم در سازمان نظام مهندسی ساختمان استان گلستان*/}
                {/*    لطفا لینک عضویت در این گروه را برای دوستان خود ارسال کنید*/}
                {/*</p>*/}
            </footer>
        </div>
    )
}
